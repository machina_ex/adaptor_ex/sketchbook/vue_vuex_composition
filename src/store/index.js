import { createStore } from 'vuex'
//import {reactive} from "vue"

export default createStore({
  strict: true,
  state: {
    count:4,
    level: null
  },
  mutations: {
    setCount: function(state,value){
      state.count = value;
    },
    setLevel: function(state,value){
      state.level = value
    },
    setName: function(state,value){
      console.log("change to:",value);
      
      state.level.chapter.chapter[0].name = value;
      console.log("from store:",state.level.chapter.chapter[0].name);
      
    }
  },
  actions: {
    initState: async function(context){
       context.commit("setLevel", await (await fetch("gordon_wird_geladen.json")).json())
    }
  },
  modules: {
  },
  getters:{
    getCount(state){return state.count}
  }
})
